/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt3_practica02;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author vesprada
 */
public class Manejador extends DefaultHandler{
    
    private String texto;
    private Receta recepta;
    private String tipo;
    private ArrayList <Receta> receptesPrincipals = new ArrayList<Receta>();
    private ArrayList <Receta> receptesSecundaris = new ArrayList<Receta>();
    private ArrayList <Receta> receptesPostres = new ArrayList<Receta>();

    public Manejador() {
        this.receptesPrincipals=receptesPrincipals;
        this.receptesSecundaris=receptesSecundaris;
        this.receptesPostres=receptesPostres;

        
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Receta getRecepta() {
        return recepta;
    }

    public void setRecepta(Receta recepta) {
        this.recepta = recepta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public ArrayList<Receta> getReceptesPrincipals() {
        return receptesPrincipals;
    }

    public void setReceptesPrincipals(ArrayList<Receta> receptesPrincipals) {
        this.receptesPrincipals = receptesPrincipals;
    }

    public ArrayList<Receta> getReceptesSecundaris() {
        return receptesSecundaris;
    }

    public void setReceptesSecundaris(ArrayList<Receta> receptesSecundaris) {
        this.receptesSecundaris = receptesSecundaris;
    }

    public ArrayList<Receta> getReceptesPostres() {
        return receptesPostres;
    }

    public void setReceptesPostres(ArrayList<Receta> receptesPostres) {
        this.receptesPostres = receptesPostres;
    }
    
    
    

    @Override
    public void startDocument() throws SAXException {
        super.startDocument(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void endDocument() throws SAXException {
        super.endDocument(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.texto="";
        if (localName.equals("receta")) {
            this.recepta = new Receta();
            this.tipo = attributes.getValue("tipo");
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equals("nombre")) {
            this.recepta.setNombre(texto);
        }
        else if(localName.equals("descripcion")){
            this.recepta.setDescripcion(texto);
        }
        else if(localName.equals("receta")){
            this.recepta.setTipo(tipo);
            if (this.recepta.getTipo().equals("principal")) {
                this.receptesPrincipals.add(this.recepta);
            }
            else if (this.recepta.getTipo().equals("secundario")) {
                this.receptesSecundaris.add(this.recepta);
            }
            else if (this.recepta.getTipo().equals("postre")) {
                this.receptesPostres.add(this.recepta);
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        this.texto= new String(ch, start, length);
    }
    
    
}
