/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt3_practica02;

import java.io.File;
import java.io.FileInputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author vesprada
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File file = new File("ejercicio1.xml");
        try{
            XMLReader procesadorXML = XMLReaderFactory.createXMLReader();
            Manejador gestor = new Manejador();
            procesadorXML.setContentHandler(gestor);
            InputSource fileXML =new InputSource(new FileInputStream(file));
            procesadorXML.parse(fileXML);
            
            XmlCtrlDom controlador = new XmlCtrlDom();
            Document doc = controlador.instanciarDocument();
            for (int i = 0; i < gestor.getReceptesPrincipals().size(); i++) {
                Element receta = doc.createElement("receta");
                receta.setAttribute("tipo", "principal");
                Element nombre = doc.createElement("nombre");
                nombre.setTextContent(gestor.getReceptesPrincipals().get(i).getNombre());
                Element descripcion = doc.createElement("descripcion");
                descripcion.setTextContent(gestor.getReceptesPrincipals().get(i).getDescripcion());
                receta.appendChild(nombre);
                nombre.appendChild(descripcion);
            }
            
        }catch(Exception e){
            
        }
    }
    
}
