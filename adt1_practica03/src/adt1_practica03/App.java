/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt1_practica03;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author etien
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, FileNotFoundException, IOException {
        Scanner teclado = new Scanner(System.in);

        int opcion;
        String marca, modelo, matricula, potencia, color;
        File f = new File("./vehiculo.bin");

        do {

            mostrarMenu();
            System.out.println("Elige una opcion: ");
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("Introduce la marca: ");
                    marca = teclado.next();
                    System.out.println("Introduce el modelo: ");
                    modelo = teclado.next();
                    System.out.println("Introduce la matricula: ");
                    matricula = teclado.next();
                    System.out.println("Introduce la potencia: ");
                    potencia = teclado.next();
                    System.out.println("Introduce el color: ");
                    color = teclado.next();

                    Vehiculo v = new Vehiculo(marca, modelo, matricula, potencia, color);
                    if (!f.exists()) {
                        ObjectOutputStream ficheroSal = new ObjectOutputStream(new FileOutputStream("./vehiculo.bin", true));
                        ficheroSal.writeObject(v);
                        ficheroSal.close();
                    } else {
                        MiObjectOutputStream sinCabecera = new MiObjectOutputStream(new FileOutputStream("./vehiculo.bin", true));
                        sinCabecera.writeObject(v);
                        sinCabecera.close();
                    }

                    break;
                case 2:
                    try {
                        ObjectInputStream ficheroRec = new ObjectInputStream(new FileInputStream("./vehiculo.bin"));
                        while (true) {
                            Vehiculo v1 = (Vehiculo) ficheroRec.readObject();

                            System.out.println(v1);

                        }
                        
                    } catch (IOException e) {

                    }

                    break;

                default:
                    System.out.println("\nOpcion incorrecta!!\n");
                    
            }

        } while (opcion != 3);

    }

    private static void mostrarMenu() {

        System.out.println("1.Insertar vehiculo");
        System.out.println("2.Visualizar vehiculos");
        System.out.println("3.Salir\n");
    }

}
