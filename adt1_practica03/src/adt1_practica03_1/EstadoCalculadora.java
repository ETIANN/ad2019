/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt1_practica03_1;

import java.io.Serializable;

/**
 *
 * @author etien
 */
public class EstadoCalculadora implements Serializable{
    private String opActiva;
    private double valor;
    private double resultado;

    public EstadoCalculadora() {
    }

    public EstadoCalculadora(String opActiva, double valor, double resultado) {
        this.opActiva = opActiva;
        this.valor = valor;
        this.resultado = resultado;
    }

    public String getOpActiva() {
        return opActiva;
    }

    public void setOpActiva(String opActiva) {
        this.opActiva = opActiva;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }

    @Override
    public String toString() {
        return "EstadoCalculadora{" + "opActiva=" + opActiva + ", valor=" + valor + ", resultado=" + resultado + '}';
    }
    
    
}
