/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt1_practica03_1;

import adt1_practica03.MiObjectOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author etien
 */
public class AlmacenamientoTemporal {
    private File fichero = new File ("./operaciones.bin");

    public AlmacenamientoTemporal() {
    }

    public File getFichero() {
        return fichero;
    }

    public void setFichero(File fichero) {
        this.fichero = fichero;
    }
    
    public void guardaOperaciones(EstadoCalculadora e) throws FileNotFoundException, IOException{
        if (!this.fichero.exists()) {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(this.fichero, true));
            oos.writeObject(e);
            oos.close();
        }
        else{
            MiObjectOutputStream moos = new MiObjectOutputStream(new FileOutputStream(this.fichero, true));
            moos.writeObject(e);
            moos.close();
        }
    }
    
    public void recuperaOperaciones() throws FileNotFoundException, IOException, ClassNotFoundException{
        try{
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(this.fichero));
        while(true){
            EstadoCalculadora e= (EstadoCalculadora)ois.readObject();
            System.out.println(e);
        }
    }catch(IOException e){
        
    }
}
}
