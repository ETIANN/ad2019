package adt1_practica03_1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author etien
 */
public class App {
    public static void main(String[] args) throws IOException, FileNotFoundException, ClassNotFoundException {
        Scanner teclado = new Scanner(System.in);
        int opc;
        double num1, num2, result;
        char op;
        String opActiva;
        AlmacenamientoTemporal at = new AlmacenamientoTemporal();
        do {
            mostrarMenu();
            System.out.println("\nIntroduce una opcion: ");
            opc=teclado.nextInt();
            
            switch (opc) {
                case 1:
                    System.out.println("Introduce el primer valor: ");
                    num1=teclado.nextDouble();
                    System.out.println("Introduce la operacion a realizar: ");
                    op=teclado.next().charAt(0);
                    System.out.println("Introduce segundo valor: ");
                    num2=teclado.nextInt();
                    
                    if (op=='+') {
                        result= num1 + num2;
                    }
                    else if (op=='-') {
                        result= num1 - num2;
                    }
                    else if (op=='*') {
                        result= num1 * num2;
                    }
                    else if (op=='/') {
                        result= num1 / num2;
                    }
                    else{
                        result=0;
                    }
                    opActiva=num1+" "+op+" "+num2;
                    EstadoCalculadora e = new EstadoCalculadora(opActiva, num2, result);
                    at.guardaOperaciones(e);
                    break;
                    
                case 2:
                    at.recuperaOperaciones();
                    break;
                default:
                    System.out.println("Opcion Incorrecta!!");;
            }
            
        } while (opc!=0);
        
    }

    private static void mostrarMenu() {
        System.out.println("1.Realizar Operacion");
        System.out.println("2.Visualizar operaciones");
        System.out.println("0.Salir");
    }
}
