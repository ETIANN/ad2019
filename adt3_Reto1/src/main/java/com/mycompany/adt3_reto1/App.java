/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.adt3_reto1;


import com.drew.imaging.ImageMetadataReader;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.iptc.IptcDirectory;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

/**
 *
 * @author etien
 */
public class App {

    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String rutaOrigen = "Imagenes/";
        int opcion;
        do {
            mostrarMenu();
            System.out.println("\nIntroduzca una opcion: ");
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1:
                    try {
                        //Indiquem el directori que anem a recorrer per a trobar les images
                        File origen = new File(rutaOrigen);
                        File[] listaImagenes = origen.listFiles();
                        for (int i = 0; i < listaImagenes.length; i++) {
                            //Recorreguem el directori
                            File imagen = new File(listaImagenes[i].getPath());
                            //Utilitzem la clase de la llibreria Metadata-extractor indicant que anem a traure la data
                            Metadata metadata = ImageMetadataReader.readMetadata(imagen);

                            ExifSubIFDDirectory directory = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
                            //Creem la carpeta amb la fecha de creacio de la image
                            Date fecha = directory.getDateOriginal();
                            String formato = "MM-yyyy";
                            SimpleDateFormat sf = new SimpleDateFormat(formato);
                            String formated = sf.format(fecha);
                            File destino = new File(formated);
                            destino.mkdir();

                            organizarImagenes(imagen, destino);

                        }
                    } catch (Exception e) {

                    }
                    break;

                case 2:
                    try {
                        //Recorreguem el directori de les images
                        File origen = new File(rutaOrigen);
                        File[] listaImagenes = origen.listFiles();
                        for (int i = 0; i < listaImagenes.length; i++) {
                            File imagen = new File(listaImagenes[i].getPath());
                            Metadata metadata = ImageMetadataReader.readMetadata(imagen);
                            //Utilitzem un metode de la llibreria per a extraure les coordenades de cada image
                            Collection<GpsDirectory> gpsDirectories = metadata.getDirectoriesOfType(GpsDirectory.class);
                            for (GpsDirectory gpsDirectory : gpsDirectories) {

                                GeoLocation geoLocation = gpsDirectory.getGeoLocation();
                                String name = gpsDirectory.getName();
                                if (geoLocation != null && !geoLocation.isZero()) {

                                    System.out.println("Nombre: " + imagen.getName() + "\tCoordenadas: " + geoLocation);

                                }
                            }

                        }

                    } catch (Exception e) {

                    }
                    break;

                case 3:

                   
                    break;

                case 4:
                    File imagen = new File(rutaOrigen.concat(elegirImagenes(rutaOrigen)));
                    reduceImagenes(imagen);
                    break;

                case 5:

                    break;
                default:
                    System.out.println("Opcion incorrecta!!");
                    ;
            }

        } while (opcion != 0);

    }

    private static void mostrarMenu() {
        System.out.println("1. Opcion 1");
        System.out.println("2. Opcion 2");
        System.out.println("3. Opcion 3");
        System.out.println("4. Opcion 4");
        System.out.println("5. Opcion 5");
        System.out.println("0. Salir");
    }

    //Metode per a copiar les images a la seua carpeta segons la data de creacio
    private static void organizarImagenes(File imagen, File destino) {
        try {
            FileInputStream fis = new FileInputStream(imagen);
            FileOutputStream fos = new FileOutputStream(destino + "/" + imagen.getName());

            byte[] buffer = new byte[fis.available()];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                fos.write(buffer, 0, length);
            }

            fis.close();
            fos.close();
        } catch (Exception e) {

        }
    }

    private static String elegirImagenes(String rutaOrigen) {
        Scanner teclado = new Scanner(System.in);
        String nombre;
        File origen = new File(rutaOrigen);
        File[] listaImagenes = origen.listFiles();
        for (int i = 0; i < listaImagenes.length; i++) {
            //Recorreguem el directori
            File imagen = new File(listaImagenes[i].getPath());
            System.out.println(imagen.getName());
        }
        System.out.println("\nElige la imagen a redimensionar");
        nombre = teclado.next();
        return nombre;
    }

    //Metode per a reduir les imatges y aplicar-li una marca de aigua
    private static void reduceImagenes(File imagen) {
        try {

            Thumbnails.of(imagen.getPath()).size(500, 500).outputFormat("jpg").watermark(Positions.BOTTOM_RIGHT, ImageIO.read(new File("MarcaAgua.jpg")), 0.5f)
                    .outputQuality(0.8).toFile(new File("Imagenes/" + "Copia" + imagen.getName()));
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
