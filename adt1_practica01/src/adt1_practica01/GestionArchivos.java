/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt1_practica01;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 *
 * @author vesprada
 */
public class GestionArchivos {

    private String nombreCarpeta;
    private String nuevaCarpeta;
    private String nombreArchivo;

    public GestionArchivos() {
        nombreCarpeta = "/";
        nuevaCarpeta = "";
        nombreArchivo = "";
    }

    public String getNombreCarpeta() {
        return nombreCarpeta;
    }

    public void setNombreCarpeta(String nombreCarpeta) {
        this.nombreCarpeta = nombreCarpeta;
    }

    public String getNuevaCarpeta() {
        return nuevaCarpeta;
    }

    public void setNuevaCarpeta(String nuevaCarpeta) {
        this.nuevaCarpeta = nuevaCarpeta;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public void listarCarpetaTrabajo() {
        File carpTrabajo = new File(nombreCarpeta);
        if (carpTrabajo.exists()) {
            File[] listaDirectorio = carpTrabajo.listFiles();
            for (File fitxer : listaDirectorio) {
                if (fitxer.isDirectory()) {
                    System.out.println(" Directori " + fitxer.getName());
                } else {
                    System.out.println(" Fitxer " + fitxer.getName());
                }
            }
        }
        else{
            System.out.println("No existe la carpeta!");
        }

    }

    public String informacionDetallada(String a) {
        File archiu = new File(a);
        String info = "";
        if (archiu.isDirectory()) {
            info = "Nombre: " + archiu.getName()
                    + "\nTipo: carpeta"
                    + "\nContenido: " + archiu.length() + " entradas"
                    + "\nUbicacion: " + archiu.getAbsolutePath()
                    + "\nUltima Modificacion: " + archiu.lastModified()
                    + "\nOculto: " + archiu.isHidden()
                    + "\nEspacio Libre: " + archiu.getFreeSpace()
                    + "\nEspacio Disponible: " + archiu.getUsableSpace()
                    + "\nEspacio Total: " + archiu.getTotalSpace();

        } else {
            info = "Nombre: " + archiu.getName()
                    + "\nTipo: fichero"
                    + "\nLongitud: " + archiu.length()
                    + "\nUbicacion: " + archiu.getAbsolutePath()
                    + "\nUltima Modificacion: " + archiu.lastModified()
                    + "\nOculto: " + archiu.isHidden();
        }
        
        return info;
    }

    public void creaCarpeta(){
        File file= new File(nombreCarpeta, nuevaCarpeta);
        file.mkdir();
    }

    public void creaFichero() throws IOException{
        File file= new File(nombreCarpeta, nombreArchivo);
        file.createNewFile();
    }

    public void eliminaFichero(){
        File file= new File(nombreCarpeta, nombreArchivo);
        file.delete();
    }
}
