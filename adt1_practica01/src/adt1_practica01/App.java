/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt1_practica01;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vesprada
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int opcion;
        String nombre;
        GestionArchivos g = new GestionArchivos();
        
        do {
            mostrarMenu();
            System.out.println("Introduce una opcion: ");
            opcion = teclado.nextInt();
            
            switch(opcion){
                case 1:
                    System.out.println("Introduce el nombre de la carpeta de trabajo: ");
                    nombre = teclado.next();
                    g.setNombreCarpeta(nombre);
                break;
                
                case 2:
                    g.listarCarpetaTrabajo();
                break;
                
                case 3:
                    System.out.println("Introduce el nombre del archivo: ");
                    nombre = teclado.next();
                    System.out.println(g.informacionDetallada(nombre));
                break;
                
                case 4:
                    System.out.println("Introduce el nombre de la carpeta a crear: ");
                    nombre = teclado.next();
                    g.setNuevaCarpeta(nombre);
                    g.creaCarpeta();
                break;
                
                case 5:
                    System.out.println("Introduce el nombre del archivo a crear: ");
                    nombre = teclado.next();
                    g.setNombreArchivo(nombre);
            {
                try {
                    g.creaFichero();
                } catch (IOException ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
                break;
                
                case 6:
                    System.out.println("Introduce el nombre del archivo a borrar: ");
                    nombre = teclado.next();
                    g.setNombreArchivo(nombre);
                    g.eliminaFichero();
                    break;
                    
                default:
                    System.out.println("Opcion incorrecta!!");
            }
            
        } while (opcion!=0);
        
    }

    private static void mostrarMenu() {
        System.out.println("1.Asignar carpeta de trabajo");
        System.out.println("2.Listado de la carpeta de trabajo");
        System.out.println("3.Información detallada de un archivo");
        System.out.println("4.Creación de un directorio");
        System.out.println("5.Creación de un fichero");
        System.out.println("6.Eliminación de un fichero");
        System.out.println("0.Salir");
    }
    
}
